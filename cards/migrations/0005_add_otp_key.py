# Generated by Django 2.1.1 on 2018-09-07 02:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cards', '0004_add_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='otp_key',
            field=models.CharField(max_length=256, null=True),
        ),
    ]
