# Generated by Django 2.1.1 on 2018-09-07 03:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cards', '0005_add_otp_key'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='otp_key',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
    ]
