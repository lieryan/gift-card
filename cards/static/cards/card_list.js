function archive(url, isActive=false, success=undefined) {
  $.ajax({
    method: 'PATCH',
    url: url,
    accepts: 'application/json',
    data: {"is_active": isActive},
    success: function(data, status, xhr) {
      let newStatus = data['is_active'];
      if (success) success(newStatus);
    }
  });
}


function update(target) {
  return function(isActive=undefined) {
    if (typeof isActive !== 'undefined') {
      target.toggleClass('object-active', isActive);
      target.toggleClass('object-inactive', !isActive);
    }
  }
}

$(function() {
  $('.object-list > tbody > tr').on('click', '.archive', function(e) {
    let detailLink = $(e.delegateTarget).data('detail-link');
    archive(detailLink, false, update(isActive=$(e.delegateTarget)));
  });

  $('.object-list > tbody > tr').on('click', '.restore', function(e) {
    let detailLink = $(e.delegateTarget).data('detail-link');
    archive(detailLink, true, update(isActive=$(e.delegateTarget)));
  });
});
