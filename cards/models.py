from django.conf import settings
from django.db import models
from passlib import totp


class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()

    def __str__(self):
        return self.name


class Card(models.Model):
    class Meta(object):
        unique_together = (('product', 'key'), )

    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    key = models.CharField(max_length=80, verbose_name='Voucher number')

    owner = models.ForeignKey('auth.User', on_delete=models.CASCADE)

    # FIXME: some currency will require more than 2 decimal places
    currency = models.CharField(max_length=3)
    value = models.DecimalField(max_digits=16, decimal_places=2)

    is_active = models.BooleanField(default=True)

    otp_key = models.CharField(max_length=256, null=True, blank=True)

    def current_pin(self):
        TOTPFactory = totp.TOTP.using(issuer='giftcard', secrets={'1': 'TOTP:' + settings.SECRET_KEY})
        if self.otp_key is None:
            otp_key = TOTPFactory.new(label=self.owner.username)
            self.otp_key = otp_key.to_json()
            self.save()
        keygen = TOTPFactory.from_json(self.otp_key)
        return keygen.generate()

    def __str__(self):
        return str(self.product) + ': ' + self.key
