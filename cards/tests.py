from decimal import Decimal

from django.contrib import auth
from django import db
from django.test import TestCase
from django.urls import reverse

from . import models


def create_product(**override):
    data = dict(
        name='Birthday Promotion',
        description='Reward to celebrate employee birthday',
    )
    data.update(override)
    return models.Product.objects.create(**data)


def create_card(product, **override):
    data = dict(
        product=product,
        key='abcdefg',
        owner_id=1,
        currency='AUD',
        value=Decimal('10.00'),
    )
    data.update(override)
    return models.Card.objects.create(**data)


class CardModelTest(TestCase):
    def test_model_create(self):
        product = create_product()
        card = create_card(product)

        self.assertIsNotNone(card.id)

    def test_card_unique_card_number(self):
        product = create_product()
        self.assertEqual(models.Card.objects.count(), 0)
        create_card(product)
        self.assertEqual(models.Card.objects.count(), 1)

        with db.transaction.atomic(), \
                self.assertRaises(db.IntegrityError):

            create_card(product)

        self.assertEqual(models.Card.objects.count(), 1)


class CardListTestMixin(object):
    expected_endpoint_url = NotImplemented
    expected_content_type = NotImplemented

    def setUp(self):
        super().setUp()
        self.user = auth.models.User.objects.create_user(username='testuser')
        self.other_user = auth.models.User.objects.create_user(username='otheruser')
        self.client.force_login(self.user)

    def test_cards_list_url(self):
        self.assertEqual(self.url, self.expected_endpoint_url)

    def test_cards_list_content_type(self):
        resp = self.client.get(self.url)
        self.assertEqual(resp['Content-Type'], self.expected_content_type)

    def do_cards_list_request(self):
        raise NotImplementedError

    def test_cards_list_data(self):
        prod1 = create_product(name="Product One")
        create_card(prod1)

        prod2 = create_product(name="Product Two")
        create_card(prod2)

        resp, object_list = self.do_cards_list_request()
        self.assertEqual(len(object_list), 2)

        with self.subTest('product name'):
            self.assertContains(resp, "Product One")
            self.assertContains(resp, "Product Two")

        with self.subTest('product description'):
            self.assertContains(resp, "Reward to celebrate employee birthday", count=2)

        with self.subTest('voucher number'):
            self.assertContains(resp, "abcdefg", count=2)

        with self.subTest('value'):
            self.assertContains(resp, "AUD", count=2)
            self.assertContains(resp, "10.00", count=2)

        with self.subTest('pin'):
            self.fail()

    def test_cards_list_not_owner(self):
        prod = create_product()
        create_card(prod, owner_id=self.other_user.id)

        resp, object_list = self.do_cards_list_request()
        self.assertEqual(len(object_list), 0)

    def test_cards_list_anonymous_user(self):
        self.client.logout()

        resp, object_list = self.do_cards_list_request()
        self.check_not_permitted(resp)


class CardListViewTest(CardListTestMixin, TestCase):
    expected_endpoint_url = '/'
    expected_content_type = 'text/html; charset=utf-8'

    def setUp(self):
        super().setUp()
        self.url = reverse('cards.list')

    def test_cards_list_template(self):
        resp = self.client.get(self.url)
        self.assertTemplateUsed(resp, 'list.html')
        self.assertTemplateUsed(resp, 'cards/card_list.html')

    def do_cards_list_request(self):
        resp = self.client.get(self.url)
        object_list = getattr(resp, 'context_data', {}).get('object_list', None)
        return resp, object_list

    def test_cards_list_data_archive(self):
        prod = create_product()
        create_card(prod, is_active=False)

        resp, object_list = self.do_cards_list_request()
        self.assertEqual(len(object_list), 1)

        self.assertContains(resp, "object-active", count=0)
        self.assertContains(resp, "object-inactive", count=1)

    def test_cards_list_data_inactive(self):
        prod = create_product()
        create_card(prod, is_active=True)

        resp, object_list = self.do_cards_list_request()
        self.assertEqual(len(object_list), 1)

        self.assertContains(resp, "object-active", count=1)
        self.assertContains(resp, "object-inactive", count=0)

    def check_not_permitted(self, resp):
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(resp['Location'], reverse('login') + '?next=' + self.url)


class CardListAPITest(CardListTestMixin, TestCase):
    expected_endpoint_url = '/card/'
    expected_content_type = 'application/json'

    def setUp(self):
        super().setUp()
        self.url = reverse('api.cards.list')

    def do_cards_list_request(self):
        resp = self.client.get(self.url)
        object_list = resp.json()
        return resp, object_list

    def check_not_permitted(self, resp):
        self.assertEqual(resp.status_code, 403)


class CardArchiveAPITest(TestCase):
    def setUp(self):
        super().setUp()

        self.user = auth.models.User.objects.create_user(username='testuser')
        self.other_user = auth.models.User.objects.create_user(username='otheruser')

        self.client.force_login(self.user)

        self.product = create_product()
        self.card = create_card(self.product, owner_id=self.user.pk)
        self.url = reverse('api.cards.detail', kwargs=dict(pk=self.card.id))

    def test_archive(self):
        self.card.is_active = True
        self.card.save()

        resp = self.client.patch(self.url, data={'is_active': False}, content_type='application/json')
        self.assertEqual(resp.status_code, 200)

        card = models.Card.objects.get(pk=self.card.pk)
        self.assertFalse(card.is_active)

    def test_restore(self):
        self.card.is_active = False
        self.card.save()

        resp = self.client.patch(self.url, data={'is_active': True}, content_type='application/json')
        self.assertEqual(resp.status_code, 200)

        card = models.Card.objects.get(pk=self.card.pk)
        self.assertTrue(card.is_active)

    def test_archive_not_owner(self):
        self.card.owner = self.other_user
        self.card.is_active = True
        self.card.save()

        resp = self.client.patch(self.url, data={'is_active': False}, content_type='application/json')
        self.assertEqual(resp.status_code, 403)

        card = models.Card.objects.get(pk=self.card.pk)
        self.assertTrue(card.is_active)
