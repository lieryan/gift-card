from rest_framework import serializers

from . import models


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta(object):
        model = models.Product
        exclude = ['url']


class CardSerializer(serializers.HyperlinkedModelSerializer):
    product = ProductSerializer()
    url = serializers.HyperlinkedIdentityField(view_name='api.cards.detail')
    class Meta(object):
        model = models.Card
        fields = ['product', 'key', 'value', 'currency', 'url']


class CardEditSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = models.Card
        fields = ['is_active']
