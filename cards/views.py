from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic.list import ListView
from rest_framework import generics, permissions

from . import models, serializers


# TODO: restrict CardList and CardListAPI to non-archived Cards only

class CardListMixin(object):
    def get_queryset(self):
        query = super().get_queryset()

        query = query.filter(owner_id=self.request.user.id)

        query = query.order_by('-is_active', 'product', 'key')

        query = query.select_related('product')

        return query


class CardList(LoginRequiredMixin, CardListMixin, ListView):
    model = models.Card

    def model_name(self):
        return self.model.__name__


class CardListAPI(CardListMixin, generics.ListAPIView):
    queryset = models.Card.objects.all()
    serializer_class = serializers.CardSerializer

    permission_classes = (permissions.IsAuthenticated, )


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view,  obj):
        return obj.owner_id == request.user.id


class CardArchiveAPI(generics.UpdateAPIView):
    queryset = models.Card.objects.all()
    serializer_class = serializers.CardEditSerializer

    permission_classes = (permissions.IsAuthenticated, IsOwner)
